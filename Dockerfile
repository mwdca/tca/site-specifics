FROM scratch
COPY * /inventory/
VOLUME /inventory
ENTRYPOINT ["/bin/true"]
